#!/bin/bash
set -x
cible=""
for vm in alice bob
do
  ./monitor.sh $vm info pci | grep v1
  if [ $? -eq 1 ]
  then
      cible=$vm
  else
    ./monitor.sh $vm device_del v1
  fi
done

sleep 0.8

./monitor.sh $cible device_add vfio-pci,host=00:1f.6,id=v1

exit 0

