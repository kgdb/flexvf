#!/bin/bash

# Qemu monitor command helper

cmd=$2
vm=$1

if [ $# -lt 2 ]
then
 echo "usage $0 <vm> <cmd>"
 exit
fi

shift
shift
args=$@

echo $prefix
virsh -c qemu:///system qemu-monitor-command $vm --hmp --cmd $cmd $args

exit 0

