#!/bin/bash

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:`pwd`/xapian-core-1.2.13/install/lib

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source ${DIR}/../configs.sh

# ssh-keygen -f ~/.ssh/id_rsa -t rsa -N ''


cat>~/.ssh/config<<EOF
Host node0
  Hostname 10.10.2.1
  User ekbrice
  StrictHostKeyChecking no
  UserKnownHostsFile /dev/null

Host base
  Hostname 10.10.2.3
  User root
  StrictHostKeyChecking no
  UserKnownHostsFile /dev/null
EOF

function wait_vm {
   while ! nc -vz 10.10.2.3 22
   do
       sleep 5
   done
   echo vm base is online
}

function launch_profile {

    ssh node0 "sudo virsh destroy base >/dev/null 2>&1"

 case $1 in
     emul)
         ssh node0 "sudo virsh create /mydata/work/models/base_emul.xml"
         wait_vm base
        ;;
    pv)
         ssh node0 "sudo virsh create /mydata/work/models/base_pv.xml"
         wait_vm base
        ;;
    vf)
         ssh node0 "sudo virsh create /mydata/work/models/base_vf.xml"
         wait_vm base
        ;;
    bm)
        ssh node0 /mydata/work/script/run_xapian.sh
        ;;
   esac;
}

for profile in emul pv vf bm
do

    launch_profile $profile

    qps=100
    while [ $qps -lt 1300 ]
    do

        NSERVERS=1
        QPS=$qps
        WARMUPREQS=1000
        REQUESTS=3000

        ssh base "cd /opt/tailbench/xapian; ./run_networked.sh" &

        sleep 10 # wait for server to boot up

        TBENCH_QPS=${QPS} TBENCH_MINSLEEPNS=100000 \
            TBENCH_TERMS_FILE=${DATA_ROOT}/xapian/terms.in \
            chrt -r 99 ./xapian_networked_client &

        wait $!

        if [ -f lats.bin ]
        then
             qps=$((qps+100))
             mv ${profile}_lats.bin lats_${qps}.bin
        fi

    done

done
