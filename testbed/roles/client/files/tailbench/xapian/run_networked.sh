#!/bin/bash


export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:`pwd`/xapian-core-1.2.13/install/lib

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source ${DIR}/../configs.sh

NSERVERS=1
QPS=500
WARMUPREQS=1000
REQUESTS=3000


qps=100

while [ $qps -lt 1300 ]
do

    ssh base "cd /opt/tailbench/xapian; ./run_networked.sh" &

    sleep 10

    TBENCH_QPS=${QPS} TBENCH_MINSLEEPNS=100000 \
        TBENCH_TERMS_FILE=${DATA_ROOT}/xapian/terms.in \
        chrt -r 99 ./xapian_networked_client &

    wait $!

    if [ -f lats.bin ]
    then
         qps=$((qps+100))
         mv lats.bin lats_$qps.bin
    fi

done
