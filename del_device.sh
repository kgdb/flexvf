#!/bin/bash

if [ $# -ne 1  ]
then
	echo  "usage $0 vm_name"
	exit 1
fi

./monitor.sh $1 device_del hostdev0

exit 0
