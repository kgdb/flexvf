#!/bin/bash
set -x
echo 'KERNEL=="kvm", NAME="%k", GROUP="kvm", MODE="0660"' > /lib/udev/rules.d/65-kvm.rules
 udevadm control --reload-rules && udevadm trigger
 rmmod kvm_intel
 rmmod kvm
 modprobe kvm
 modprobe kvm_intel
exit 0
