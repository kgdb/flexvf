#!/bin/bash

function build_table {
while read line
do
   ips=""
   while read field
   do 
      ips="$ips,$(awk '{print $4}' <<< $field | cut -d '/' -f 1 )"
   done <<< $(virsh domifaddr $line | grep ip)
   echo "$line$ips,4"
done <<< $(virsh list | grep running | awk '{print $2}')
}
build_table
exit 0
