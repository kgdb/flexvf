#!/bin/bash
#id=$(lshw -c network -businfo | grep -i Ille | awk '{print $1}' | cut -d '@' -f 2)

#attach nic virtual funcion to guest

id=0000:06:10.1
vm=$1
if [ $# -ne 1 ]
then
        echo "usage ./add_device vm_name"
        exit 1
fi

./monitor.sh $vm device_add vfio-pci,host=$id,id=hostdev0
exit 0

