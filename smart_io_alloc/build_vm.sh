#!/bin/bash

set -e 

function teamd_rules {
cat>/tmp/999-eth.rules<<EOF
KERNEL=="ens7", ACTION=="move", RUN+="/usr/bin/teamd_hotplug"
KERNEL=="ens7", ACTION=="remove", RUN+="/usr/bin/teamd_hotplug"
EOF
}


#handle nic event and update teamd
function udev_teamd_handler 
{
cat>/tmp/teamd_hotplug<<EOF
#!/bin/bash

#ip link set dev ens7 down

#sleep 0.3

teamdctl team0 state | grep eth0
if [ $? -eq 0 ]
then
	teamdctl team0 port add ens7
	ip link set dev ens7 up
        ip link set eth0 dev down    
	teamdctl team0 port remove eth0
else
	teamdctl team0 port add eth0
	ip link set dev eth0 up
        ip link set ens7 dev down    
	teamdctl team0 port remove ens7
fi 

exit 0
EOF
chmod +x /tmp/teamd_hotplug
}

# create teamd confi
function team_config {
cat>/tmp/team0.conf<<EOF
{
   "device": "team0",
   "runner": {"name": "activebackup"},
   "link_watch": {"name": "ethtool"},
   "ports": {
      "eth0": {
         "prio": -10,
         "sticky": true
       },
       "ens7": {
         "prio": 100
       }
   }
}
EOF
cat>/tmp/ifcfg-team0-slave0<<EOF
NAME=team0-eth0
DEVICE=eth0
ONBOOT=yes
NM_CONTROLLED=no
TEAM_MASTER=team0
DEVICETYPE=TeamPort
EOF

cat>/tmp/ifcfg-team0-slave1<<EOF
NAME=team0-ens7
DEVICE=ens7
ONBOOT=yes
TEAM_MASTER=team0
NM_CONTROLLED=no
DEVICETYPE=TeamPort
EOF


cat>/tmp/ifcfg-team0<<EOF
DEVICE=team0
NAME=team0
DEVICETYPE=Team
ONBOOT=yes
BOOTPROTO=none
IPADDR=10.10.2.3
PREFIX=24
NM_CONTROLLED="no"
TEAM_CONFIG='{"runner": {"name": "activebackup"}, "link_watch": {"name": "ethtool"}}'
EOF
}

# build virtual machine
function build {
    
    echo toto >/tmp/rootpwd
    team_config
    udev_teamd_handler 
    teamd_rules 

    for vm in alice
    do
        virt-builder centos-7.8 \
            -o ${vm}_disk.img \
            --size 20G \
            --root-password file:/tmp/rootpwd \
            --hostname $vm \
            --install "vim,git,epel-release,teamd,lshw,polkit,boost-devel,readline-devel,gperftools-libs, wget,cmake3,jemalloc-devel,gperftools-devel,numactl-devel,libgtop2-devel,java-1.8.0-openjdk-devel,ant, python-devel,python-pip,scipy,numpy,bcc,bpftool,yum-utils,cloud-utils-growpart,systemtap,kernel-devel,iperf3,kernel" \
	    --firstboot-command 'ldconfig; dhclient -v eth1; debuginfo-install kernel -y; yum groupinstall "Development Tools" -y' \
            --copy-in /tmp/teamd_hotplug:/usr/bin \
            --copy-in /tmp/team0.conf:/etc \
            --copy-in /tmp/999-eth.rules:/etc/udev/rules.d \
	    --ssh-inject root:file:/users/ekbrice/.ssh/id_rsa.pub \
	    --upload /mydata/smart_io_alloc/dev.tar:/root/dev.tar \
	    --copy-in /tmp/ifcfg-team0-slave1:/etc/sysconfig/network-scripts \
	    --copy-in /tmp/ifcfg-team0-slave0:/etc/sysconfig/network-scripts \
	    --copy-in /tmp/ifcfg-team0:/etc/sysconfig/network-scripts
    done
}

function import_vm {
vm=$1
virt-install \
    --virt-type=kvm \
    --noacpi \
    --noapic \
    --name $vm \
    --ram 8192 \
    --vcpus=4 \
    --os-variant=rhel7 \
    --network network=default,model=virtio \
    --nographics  \
    --disk path=./$vm_disk.img,size=2,bus=virtio \
    --update
}

case $1 in
	build)
		build
		;;
	import)
		import_vm $2
		;;
esac;

exit 0
