#!/bin/bash
case $1 in
    yes)
        exit 0
        ;;
esac;

if [ ! -f sockperf ]
then
    git clone https://github.com/Mellanox/sockperf.git
fi

pushd sockperf

./autogen.sh && ./configure --enable-tool --prefix=/usr &&  make &&  make install

popd

exit 0
