#!/bin/bash

#apt-get install -y gperf genisoimage libncurses5-dev libaugeas-dev libmagic-dev libjansson-dev libhivex-dev supermin ocaml libhivex-ocaml-dev cpio bison flex po4a liblzma-dev xz-utils


echo $1 > /tmp/fs.txt

case $1 in
    yes)
        exit 0
        ;;
esac;

if [ ! -f libguestfs ]
then
    git clone https://github.com/libguestfs/libguestfs
fi

pushd libguestfs
git submodule update --init
CFLAGS=-fPIC ./autogen.sh
./configure --prefix=/usr
make -j 32 && make install
popd

pushd libguestfs/website/download/builder
 wget http://builder.libguestfs.org/centos-7.2.xz
 wget http://builder.libguestfs.org/cirros-0.3.5.xz
 wget http://builder.libguestfs.org/ubuntu-18.04.xz
popd

exit 0
