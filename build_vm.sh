#!/bin/bash

set -e

function teamd_rules {
cat>/tmp/999-eth.rules<<EOF
KERNEL=="eth1", SUBSYSTEMS=="net", ACTION=="add", RUN+="/usr/bin/teamd_hotplug"
EOF
}


#handle nic event and update teamd
function udev_teamd_handler
{
cat>/tmp/teamd_hotplug<<EOF
#!/bin/bash
for eth in eth1 eth2
do
    ip link set $eth down
    ip link set $eth down
done
teamd -g -f /etc/team0.conf -d
exit 0
EOF
}

# create teamd confi
function team_config {
cat>/tmp/team0.conf<<EOF
{
   "device": "team0",
   "runner": {"name": "activebackup"},
   "link_watch": {"name": "ethtool"},
   "ports": {
      "eth2": {
         "prio": -10,
         "sticky": true
       },
       "eth1": {
         "prio": 100
       }
   }
}
EOF
}



# build virtual machine
function build {

    echo toto >/tmp/rootpwd
    team_config
    udev_teamd_handler
    teamd_rules

    for vm in fuck
    do
        virt-builder -v -x centos-7.3 \
            -o ${vm}_disk.img \
            --size 6G \
            --root-password file:/tmp/rootpwd \
            --hostname $vm \
            --install "vim,git,team,epel-release,teamd,lshw" \
            --copy-in /tmp/teamd_hotplug:/usr/bin \
            --copy-in /tmp/team0.conf:/etc \
            --copy-in /tmp/999-eth.rules:/etc/udev/rules.d
    done
}

function import_vm {
  for vm in alice bob
  do
virt-install \
    --virt-type=kvm \
    --name $vm \
    --ram 8192 \
    --vcpus=4 \
    --os-variant=rhel7 \
    --hvm \
    --network network=default,model=virtio \
    --nographics  \
    --disk path=./$vm_disk.img,size=2,bus=virtio \
    --import
  done
}


build

exit 0
