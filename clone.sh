#!/bin/bash

#virsh suspend alice

for i in $(seq 10)
do 
   virsh destroy vm_$i >/dev/null 2>&1
   virsh undefine vm_$i >/dev/null 2>&1
done

for i in $(seq 10)
do
 #virt-clone --original alice --name vm_${i} --file `pwd`/vm_disk_${i}.qcow2
 
qemu-img create -f qcow2 -F qcow2 -b tpl.qcow2 vm_disk_${i}.qcow2

 cp tpl.xml vm_${i}.xml
 mac0=$(echo -n aa:00:00:`openssl rand -hex 3 | sed 's/\(..\)/\1:/g; s/.$//'`)
 mac1=$(echo -n aa:00:01:`openssl rand -hex 3 | sed 's/\(..\)/\1:/g; s/.$//'`)
 sed -i -e "s/DOMAIN/vm_$i/" vm_${i}.xml
 sed -i -e "s/UUID/`uuidgen`/" vm_${i}.xml
 sed -i -e "s/MEM/4194304/g" vm_${i}.xml
 sed -i -e "s/DISK/vm_disk_${i}/g" vm_${i}.xml
 sed -i -e "s/MAC0/${mac0}/g" vm_${i}.xml
 sed -i -e "s/MAC1/${mac1}/g" vm_${i}.xml
 sed -i -e "s/VNET0/vmnetA${i}/g" vm_${i}.xml
 sed -i -e "s/VNET1/vmnetB${i}/g" vm_${i}.xml
 sed -i -e "s/CHANEL/vm_${i}/g" vm_${i}.xml
 sed -i -e "s/CPUS/4/g" vm_${i}.xml

 virsh create vm_${i}.xml
 virsh shutdown --mode acpi vm_${i}
done
exit 
for i in $(seq 10)
do
 #virt-sysprep -d vm_${i} --hostname vm_${1}
 #virsh destroy vm_${i}
echo 
done

exit 0
