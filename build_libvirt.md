
compiler:
 gcc-7


# Libvirt 
packages:
 - libgnutls28-dev
 - libnl-3-dev
 - libnl-route-3-dev
 - libxml2-dev
 - docutils-common
 - xsltproc
 - libdevmapper-dev
 - uuid-dev
 - libglib2.0-dev
 - libpciaccess-dev

git clone https://github.com/libvirt/libvirt.git
wget https://libvirt.org/sources/libvirt-6.5.0.tar.xz

tar xvf libvirt-6.4.0.tar.xz 

cd libvirt-6.4.0

mkdir build

cd build

../configure

make -j 32 && make install

# Qemu 

packages:
 - libpixman-1-dev 

git clone https://github.com/qemu/qemu.git

wget https://download.qemu.org/qemu-4.2.1.tar.xz

tar xvf qemu-4.2.1.tar.xz 

cd qemu-4.2.1

mkdir -p bin/debug/native

cd bin/debug/native

../../../configure --enable-system  --enable-debug --disable-docs --target-list=x86_64-softmmu --prefix=/usr
make clean && make -j 32 && make install

systemctl unmask virtlockd.socket 
systemctl unmask virtlogd.socket
systemctl unmask libvirtd.socket


systemctl enable virtlockd.socket 
systemctl enable virtlogd.socket
systemctl enable libvirtd.socket
