from __future__ import (absolute_import, division, print_function,
                        unicode_literals)
from bcc import BPF
import sys
import ctypes
from bcc import DEBUG_PREPROCESSOR
import socket
import struct
from struct import pack
from socket import inet_ntop, AF_INET, AF_INET6
from collections import namedtuple, defaultdict
from time import sleep, strftime
from multiprocessing import Process
from multiprocessing.managers import BaseManager
import os

TCP_IP = "192.168.122.1"
TCP_PORT = 7000

prog =r"""
#include<linux/inet.h>

#include<linux/jiffies.h>
#include <uapi/linux/ptrace.h>
#include <net/sock.h>
#include <net/tcp_states.h>
#include <bcc/proto.h>

#include<asm/ptrace.h>
#include <uapi/linux/ptrace.h>
#include <uapi/linux/limits.h>



struct ipv4_key_t {
    u32 pid;
    u32 saddr;
    u32 daddr;
    u16 lport;
    u16 dport;
};

BPF_HASH(ipv4_send_bytes, struct ipv4_key_t);

int inet_sendmsg(struct pt_regs *ctx, struct kiocb *iocb, struct sock *sk, struct msghdr *msg,size_t size){
    u32 pid = bpf_get_current_pid_tgid() >> 32;
    u16 dport = 0, family = sk->__sk_common.skc_family;
   
    if( 1 ) {
        struct ipv4_key_t ipv4_key = {.pid = pid};
        ipv4_key.saddr = sk->__sk_common.skc_rcv_saddr;
        ipv4_key.daddr = sk->__sk_common.skc_daddr;
        ipv4_key.lport = sk->__sk_common.skc_num;
        dport = sk->__sk_common.skc_dport;
        ipv4_key.dport = ntohs(dport);
        //ipv4_send_bytes.increment(ipv4_key, size);
         
       u64 zero = 0, *val;
       val = ipv4_send_bytes.lookup_or_init(&ipv4_key, &zero);
       (*val) += size;
    
       
    }
    return 0;
}

"""

class ipv4_key_t(ctypes.Structure):
    _fields_ = [
                ("pid", ctypes.c_uint32),
                ("saddr", ctypes.c_uint32),
                ("daddr", ctypes.c_uint32),
                ("lport", ctypes.c_uint16),
                ("dport", ctypes.c_uint16)]


TCPSessionKey = namedtuple('TCPSession', ['pid', 'laddr', 'lport', 'daddr', 'dport'])

def get_ipv4_session_key(k):
    return TCPSessionKey(pid=k.pid,
                         laddr=inet_ntop(AF_INET, pack("I", k.saddr)),
                         lport=k.lport,
                         daddr=inet_ntop(AF_INET, pack("I", k.daddr)),
                         dport=k.dport)

def build_dict(ipv4_send_bytes):
    #ipv4_throughput = defaultdict(lambda: 0)
    ipv4_throughput = {}
    for k, v in ipv4_send_bytes.items():
        key = get_ipv4_session_key(k)
        ipv4_throughput[key] = v.value
    ipv4_send_bytes.clear()
    
    
    return ipv4_throughput

def show_stats( ipv4_throughput ):
    if ipv4_throughput:
        print("%-6s  %-21s %-21s %6s" % ("PID",
            "LADDR", "RADDR", "TX_KB"))
    # output
    for k, send_bytes in sorted(ipv4_throughput.items(),
                                              key=lambda kv: sum(kv[1]),
                                              reverse=True):
        print("%-6d %-21s %-21s %6d" % (k.pid,
            k.laddr + ":" + str(k.lport),
            k.daddr + ":" + str(k.dport), int(send_bytes / 1024)))

def setup_probe():
    bpf = BPF(text=prog)
    bpf.attach_kprobe(event="inet_sendmsg", fn_name="inet_sendmsg")
    return bpf


def forward_host(ipv4_send_bytes):
    stats = build_dict(ipv4_send_bytes)
    values = []
    for k,v in stats.items():
       values.append(k.pid)
       values.append(v)
    print(values)
    MESSAGE = pack(b"@I", len(values))
    MESSAGE += struct.pack(b"@%sf"% len(values),*values)

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((TCP_IP,TCP_PORT))
    sock.send(bytes(MESSAGE))
    sock.close()

def suspend_apps():
    UDP_IP = "0.0.0.0"
    UDP_PORT = 5005
    sock = socket.socket(socket.AF_INET, # Internet
                 socket.SOCK_DGRAM) # UDP
    sock.bind((UDP_IP, UDP_PORT))
    while True:
        data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
        cmd = data.split(' ')
        os.system('kill -%s %s' % (cmd[0], cmd[1]) )

def run(bpf):
    ipv4_send_bytes = bpf["ipv4_send_bytes"]
    exiting = False
    while  not exiting:
        try:
            sleep(1)
        except KeyboardInterrupt:
            exiting = True
        try:
            forward_host(ipv4_send_bytes)
        except Exception as  e:
            pass

def watch_device():
    os.system("python device_watch.py -b team0 -p enp0s2f1 -t eth0")


if __name__ == "__main__":
        print(sys.argv)
        TCP_PORT  = int(sys.argv[1])
        bpf = setup_probe()
        p = Process(target=suspend_apps, args=())
        p.start()
        w = Process(target=watch_device, args=())
        w.start()
        run(bpf)
