"""sr-iov test"""

#
# NOTE: This code was machine converted. An actual human would not
#       write code like this!
#

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import the Emulab specific extensions.
import geni.rspec.emulab as emulab

# Create a portal object,
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

# Node node-0
node_0 = request.RawPC('node-0')
node_0.hardware_type = 'c220g2'
img = 'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD'
img = 'urn:publicid:IDN+wisc.cloudlab.us+image+legoos-deploy-PG0:sriov-kvm.node-0'

node_0.disk_image = img

# Node node-1
node_1 = request.RawPC('node-1')
node_1.hardware_type = 'c220g2'
node_1.disk_image = img

bs = node_0.Blockstore("bs", "/mydata")
bs.size = "80GB"

iface = node_0.addInterface()
fsnode = request.RemoteBlockstore("fsnode", "/mydata")
fsnode.dataset = "urn:publicid:IDN+wisc.cloudlab.us:legoos-deploy-pg0+ltdataset+vms_spaces"

#connect storage
fslink = request.Link("fslink")
fslink.addInterface(iface)
fslink.addInterface(fsnode.interface)

#connect pcs
iface1 = node_0.addInterface()
iface2 = node_1.addInterface()

link1 = request.Link("link1")
link1.addInterface(iface1)
link1.addInterface(iface2)

fslink.best_effort = True
fslink.vlan_tagging = True

portal.context.printRequestRSpec()

# Print the generated rspec
pc.printRequestRSpec(request)
