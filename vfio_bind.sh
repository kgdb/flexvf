#!/bin/bash
set -x
modprobe vfio-pci


function bind_device {
    for dev in "$@"; do
            vendor=$(cat /sys/bus/pci/devices/$dev/vendor)
            device=$(cat /sys/bus/pci/devices/$dev/device)
            if [ -e /sys/bus/pci/devices/$dev/driver ]; then
                    echo $dev > /sys/bus/pci/devices/$dev/driver/unbind
            fi
            echo $vendor $device > /sys/bus/pci/drivers/vfio-pci/new_id
    done
}

function do_it {
    for id in $@
    do
       bind_device "$id"
    done
}

do_it $@

exit 0

