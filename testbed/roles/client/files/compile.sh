#!/bin/bash

pushd tailbench/$1
./build.sh
popd

chmod -R 777 tailbench/$1

exit 0
