#!/bin/bash

set -x
#sudo brctl addbr br0

iface=$(ip a s | grep 10.10 | awk '{print $7}')

pci_id=$(lshw -c network -businfo | grep $iface | awk '{print $1}' | cut -d '@' -f 2)

./enable_vf.sh $iface


target=$(lshw -c network -businfo | grep Illegal | awk '{print $1}' | cut -d '@' -f 2)

./vfio_bind.sh $target

ip=$(ip a s | grep 10.10 | awk '{print $2}')
ip addr del $ip dev $iface
ip link set dev $iface down


a=$(virsh net-list | grep sriovbr | awk '{print $1}')
if [ ! -z $a ]
then 
   virsh net-destroy sriovbr
fi 
virsh net-define br0.xml
virsh net-start sriovbr

brctl delif br0 br0-nic

#ip addr add $ip dev br0
brctl addif br0 $iface

ifconfig $iface 0.0.0.0 up

pcis=$(lshw -c network -businfo | grep -i Ille | awk '{print $1}' | cut -d '@' -f 2)
echo $pcis

exit 0
