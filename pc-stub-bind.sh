#!/bin/bash

cat>/dev/null<<EOF
02:00.1 Ethernet controller [0200]: Broadcom Inc. and subsidiaries NetXtreme BCM5720 Gigabit Ethernet PCIe [14e4:165f]
	Subsystem: Dell NetXtreme BCM5720 Gigabit Ethernet PCIe [1028:0639]
	Kernel driver in use: tg3
	Kernel modules: tg3
EOF


echo "14e4 165f" > /sys/bus/pci/drivers/pci-stub/new_id
echo "0000:02:00.1" > /sys/bus/pci/devices/0000:02:00.1/driver/unbind
echo "0000:02:00.1" > /sys/bus/pci/drivers/pci-stub/bind


exit 0
