# -*- coding: future_fstrings -*-
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)
from bcc import BPF
import os
import getopt
import sys

prog =r"""

#include <linux/pci.h>
#include <linux/mod_devicetable.h>

BPF_PERF_OUTPUT(output_add);
BPF_PERF_OUTPUT(output_remove);

int ixgbevf_probe(struct pt_regs *ctx, struct pci_dev *pdev, const struct pci_device_id *ent)
{
  int n = 1;
  output_add.perf_submit(ctx, &n, sizeof(int)); 
  return 0;
}

int ixgbevf_remove(struct pt_regs *ctx, struct pci_dev *pdev)
{

  int n = 0;
  output_remove.perf_submit(ctx, &n, sizeof(int)); 
  return 0;
}


"""

team="team0"
transient="eth0"
persistent="ens2f1"

import ctypes 

def handle_event_probe(ctx, n, s):
    os.system(f"ifconfig {transient} down; teamdctl {team} port add {transient}; ifconfig {transient} up; ifconfig {persistent} down")
    print("device added !!")
    pass

def handle_event_remove(ctx, n, s):
    os.system(f"ifconfig {persistent} up")
    print("device removed !!")
    pass

def usage(cmd):
    print(f"{cmd} -b <team_name> -p <persistent_nic> -t <transient_nic>")
    pass

def parseargs():
    try:
            opts, args = getopt.getopt(sys.argv[1:], "b:t:p:")
    except getopt.GetoptError as err:
        print(err)  # will print something like "option -a not recognized"
        usage(sys.argv[0])
        sys.exit(2)
    if len(opts) < 3 :
        usage(sys.argv[0])
        sys.exit(1)

    for o, a in opts:
        if o == "-b":
            team = a
        elif o in ("-t"):
            transient = a
        elif o in ("-p"):
            persistent = a
        else:
            assert False, "unhandled option"

def setup_probe():
	b = BPF(text=prog)
	b.attach_kretprobe(event="ixgbevf_probe", fn_name="ixgbevf_probe")
	b.attach_kretprobe(event="ixgbevf_remove", fn_name="ixgbevf_remove")
        b["output_add"].open_perf_buffer(handle_event_probe)
        b["output_remove"].open_perf_buffer(handle_event_remove)
	return b

def run(bpf):
    while True:
     try:
         bpf.kprobe_poll()
     except KeyboardInterrupt:
         sys.exit(0)

if __name__ == "__main__":
        parseargs()
	bpf = setup_probe()
        run(bpf)