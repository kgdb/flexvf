#!/bin/sh

MONITORDIR=`pwd`

inotifywait -m -r -e close_write --format '%w%f' "${MONITORDIR}" | while read NEWFILE ACTION FILE
do
        dir=$(basename `pwd`)
        rsync -av ${NEWFILE} p1:$dir 
done