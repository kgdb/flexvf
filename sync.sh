#!/bin/sh

MONITORDIR="/home/labs/sr-iov/smart_io_alloc"

inotifywait -m -r -e close_write --format '%w%f' "${MONITORDIR}" | while read NEWFILE ACTION FILE
do
        rsync -av ${NEWFILE} p1:/mydata/smart_io_alloc  
done
