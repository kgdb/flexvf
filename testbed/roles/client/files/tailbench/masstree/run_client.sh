#!/bin/bash

if [[ -z "${NTHREADS}" ]]; then NTHREADS=1; fi

QPS=1000 \
MAXREQS=3000 \
WARMUPREQS=14000 \
TBENCH_SERVER=10.10.2.3 \
TBENCH_QPS=${QPS} TBENCH_MINSLEEPNS=10000 chrt -r 99 ./mttest_client_networked &
pid=$!

kill -9 $pid >/dev/null 2>&1

