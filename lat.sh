#!/bin/bash

netperf -H 10.10.2.2 -l 100 -t TCP_RR -v 2 -w 10ms -b 1 -- -O min_latency,mean_latency,max_latency,stddev_latency,transaction_rate


exit 0
