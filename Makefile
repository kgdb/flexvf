all: smart

CFLAGS=-std=gnu99 -g -I/usr/include/glib-2.0 -I/usr/lib/glib-2.0/include -I/usr/lib/x86_64-linux-gnu/glib-2.0/include

NODENAME := $(shell uname -n)

ifeq ($(NODENAME),archlinux)
LDFLAGS= -luv -lpthread -ldl -lrt -lglib-2.0
else
LDFLAGS= -L/usr/local/lib -luv -lpthread -ldl -lrt -lglib-2.0
endif


smart: smart_alloc.o rbtree.o
	gcc -o  smart  $? $(LDFLAGS)

rbtree.o: rbtree.c
	gcc $(CFLAGS) -c rbtree.c

smart_alloc.o: smart_alloc.c
	gcc $(CFLAGS) -c smart_alloc.c

clean:
	rm -rf  *.o smart
