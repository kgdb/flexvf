#!/bin/bash
# Qemu

# apt-get install -y  libpixman-1-dev libspice-protocol-dev libspice-server-dev

echo $1 > /tmp/qemu.txt

case $1 in
    yes)
        exit 0
        ;;
esac;

if [ ! -f qemu ]
then
    git clone https://github.com/qemu/qemu.git
fi
mkdir -p qemu/bin/debug/native

pushd qemu/bin/debug/native

../../../configure --enable-system  --enable-debug --disable-docs --target-list=x86_64-softmmu --prefix=/usr  --enable-spice --enable-kvm --enable-hax

make clean && make -j 32 && make install

popd

qemu-system-x86_64 --version


exit 0
