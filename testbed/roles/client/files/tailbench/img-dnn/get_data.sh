#!/bin/bash

mkdir mnist
pushd mnist
wget http://yann.lecun.com/exdb/mnist/train-images-idx3-ubyte.gz
wget http://yann.lecun.com/exdb/mnist/train-labels-idx1-ubyte.gz
wget http://yann.lecun.com/exdb/mnist/t10k-images-idx3-ubyte.gz
wget http://yann.lecun.com/exdb/mnist/t10k-labels-idx1-ubyte.gz

for f in `ls *.gz`
do
  gunzip $f
done
popd

pushd models
tar xvf model_500pts_80kiters.tgz
mv model_500pts_80kiters.xml model.xml
rm model_500pts_80kiters.tgz
popd
exit 0
