#define _GNU_SOURCE
#include "log.h"
#include "rbtree.h"
#include <gmodule.h>
#include <search.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/queue.h>
#include <unistd.h>
#include <uv.h>

#include <assert.h>
#include <linux/futex.h>
#include <sys/syscall.h>

#include <libvirt/libvirt.h>

#define HASH_SIZE 1000
#define SERVER "10.10.2.1"
#define PORT 7000
#define BOSOT_TREHHOLD (2 << 13)
#define SCHED_FREQ 15000
#define SERVICE_TIME 100 // 100 ms time service

// https://github.com/esnet/iperf.git
// reload udev: udevadm control --reload-rules && udevadm trigger

int futex_var = 0;

char *current_vm;

uv_loop_t *loop;

uv_loop_t *sched_loop;

struct sockaddr_in addr;

virConnectPtr hypervisor;
/*
const char *interface =
    "<interface type='hostdev' managed='no'>"
    " <mac address='52:54:00:b0:6e:21'/> "
    " <driver name='vfio'/>"
    "   <source> "
    "     <address type='pci' domain='0' bus='6' slot='0x10' function='1'/> "
    "   </source> "
    " <model type='virtio'/>"
    " <teaming type='transient' persistent='ua-backup0'/> "
    " <virtualport type='802.1Qbh'> "
    "  <parameters profileid='finance'/> "
    " </virtualport> "
    " </interface> ";
*/
const char *interface =
    "<interface type='hostdev' managed='no'>"
    " <mac address='52:54:00:b0:6e:21'/> "
    " <driver name='vfio'/>"
    "   <source> "
    "     <address type='pci' domain='0' bus='6' slot='0x10' function='0x1'/> "
    "   </source> "
    " <teaming type='transient' persistent='ua-backup0'/> "
    " <alias name='hostdev0'/>"
    " </interface> ";

#define futex(a, b, c, d, e, f) syscall(SYS_futex, a, b, c, d, e, f)

#define futex_wait(addr, val) syscall(SYS_futex, addr, FUTEX_WAIT, val, NULL)
#define futex_wakeup(addr, nb) syscall(SYS_futex, addr, FUTEX_WAKE, nb)

static double elapsed_realtime(void)
{ // returns 0 seconds first time called
  static struct timespec t0;
  struct timespec tv;
  clock_gettime(CLOCK_REALTIME, &tv);
  if (!t0.tv_sec)
    t0 = tv;
  return tv.tv_sec - t0.tv_sec + (tv.tv_nsec - t0.tv_nsec) / 1000000000.;
}

static int duration(double t1, double t2)
{
  double tdiff = t2 - t1;
  double nstime;
  nstime = tdiff * 1e9 / 1.0;
  if (nstime < 1000) {
    log_info("elapsed: %.3f (ns)\n", nstime);
  }
  else if (nstime < 1e6) {
    log_info("elapsed: %.3f (us)\n", nstime / 1000);
  }
  else if (nstime < 1e9) {
    log_info("elapsed: %.3f (ms)\n", nstime / 1e6);
  }
  else {
    log_info("elapsed: %.3f (s)\n", nstime / 1e9);
  }
  return 0;
}

//=================== FUT£X
static void LOCK(int *f)
{
  int old;
  old = __sync_val_compare_and_swap(f, 0, 1);
  switch (old) {
  case 0: {
    return;
  } break;
  default: {
    old = __sync_lock_test_and_set(f, 2);
    while (old != 0) {
      futex_wait(f, 2);
      old = __sync_lock_test_and_set(f, 2);
    }
  } break;
  }
}

static void UNLOCK(int *f)
{
  int old;
  old = __sync_fetch_and_sub(f, 1);
  switch (old) {
  case 1: {
  } break;
  case 2: {
    old = __sync_lock_test_and_set(f, 0);
    futex_wakeup(f, 1);
  } break;
  default:;
  }
}
//=================== FUTEX

//=================== GHASH
GHashTable *vms_table;
typedef struct sz_data_ {
  size_t size;
  char *value;
  unsigned int count;
} sz_data_t;

// fwd decl

sz_data_t *table_get(GHashTable *table, char *key);

GHashTable *table_create() { return g_hash_table_new(g_str_hash, g_str_equal); }

void table_free_hash_entry(gpointer key, gpointer value, gpointer user_data)
{
  g_free(key);
  free(value);
}

void table_print_hash_entry(gpointer key, gpointer value, gpointer user_data)
{
  sz_data_t *d = (sz_data_t *)value;
  log_trace("[%s] -> {name:%s,count:%d} \n", (char *)key, d->value, d->count);
}

int table_add(GHashTable *table, char *key, sz_data_t *data)
{
  g_hash_table_insert(table, (gpointer)key, (gpointer)data);
  return 0;
}

void table_print(GHashTable *table)
{
  log_trace("vm_tables : \n");
  g_hash_table_foreach(table, table_print_hash_entry, NULL);
  log_trace("\n");
}

int table_update_inc_count(GHashTable *table, char *key)
{
  sz_data_t *tmp = table_get(table, key);
  if (tmp) {
    tmp->count += 1;
    g_hash_table_replace(table, g_strdup(key), tmp);
  }
  return 0;
}

int table_update_dec_count(GHashTable *table, char *key, int count)
{
  sz_data_t *tmp = table_get(table, key);
  if (tmp) {
    tmp->count -= count;
    g_hash_table_replace(table, g_strdup(key), tmp);
  }
  return 0;
}

sz_data_t *table_get(GHashTable *table, char *key)
{
  return (sz_data_t *)g_hash_table_lookup(table, (gconstpointer)key);
}

void table_destroy(GHashTable *table)
{
  g_hash_table_foreach(table, table_free_hash_entry, NULL);
  g_hash_table_destroy(table);
}

//=================== GHASH

//=================== RBTREE
struct stats_info {
  unsigned int pid;
  unsigned long bytes_send;
  unsigned long timestamp;
  char *ip;
};

struct stats_node {
  struct rb_node node;
  struct stats_info stat;
};

struct rb_root stats_tree = RB_ROOT;
struct stats_node *search(struct rb_root *root, unsigned long bytes)
{
  struct rb_node *node = root->rb_node;
  while (node) {
    struct stats_node *data = container_of(node, struct stats_node, node);
    if (data->stat.bytes_send > bytes)
      node = node->rb_left;
    else if (data->stat.bytes_send < bytes)
      node = node->rb_right;
    else
      return data;
  }
  return NULL;
}

int insert(struct rb_root *root, struct stats_node *data)
{
  struct rb_node **new = &(root->rb_node), *parent = NULL;
  while (*new) {
    struct stats_node *this = container_of(*new, struct stats_node, node);
    parent = *new;
    if (data->stat.bytes_send > this->stat.bytes_send)
      new = &((*new)->rb_left);
    else if (data->stat.bytes_send < this->stat.bytes_send)
      new = &((*new)->rb_right);
    else
      return 0;
  }
  rb_link_node(&data->node, parent, new);
  rb_insert_color(&data->node, root);
  return 1;
}

void print_stats(struct rb_root *root)
{
  struct rb_node *node;
  log_trace("all nodes: \n");
  for (node = rb_first(root); node; node = rb_next(node))
    log_trace("%s -> bytes_send = %ld\n",
              rb_entry(node, struct stats_node, node)->stat.ip,
              rb_entry(node, struct stats_node, node)->stat.bytes_send);
}

void free_memory(struct stats_node *node)
{
  if (node != NULL) {
    fre(node);
    node = NULL;
  }
}
//=================== RBTREE

static char *get_peername(uv_tcp_t *t)
{
  char *result = malloc(51);
  struct sockaddr_storage addr;
  memset(&addr, 0, sizeof(addr));
  int alen = sizeof(addr);
  int r = uv_tcp_getpeername(t, (struct sockaddr *)&addr, &alen);
  if (r == 0) {
    char ip[INET_ADDRSTRLEN] = {0};
    int port = 1;
    if (1) {
      struct sockaddr_in *addr_i4 = (struct sockaddr_in *)&addr;
      uv_ip4_name(addr_i4, ip, sizeof ip);
      // inet_ntop(AF_INET, &addr.sin_addr.s_addr, ip, INET_ADDRSTRLEN);
      port = addr_i4->sin_port;
    }
    // sprintf(result, "%s:%d", ip, port);
    sprintf(result, "%s", ip);
  }
  return result;
}

//==================== FIFO
STAILQ_HEAD(vmsqueue, vm_entry)
boosted_head = STAILQ_HEAD_INITIALIZER(boosted_head);
struct vmsqueue boosted_vms;

struct vm_entry {
  STAILQ_ENTRY(vm_entry)
  entries;
  char *vm_name;
} * vm_new, *n1, *n2;

void add_boosted(char *vm_name)
{
  vm_new = malloc(sizeof(struct vm_entry)); /* Insert at the tail. */
  vm_new->vm_name = strdup(vm_name);
  STAILQ_INSERT_TAIL(&boosted_head, vm_new, entries);
  // log_info("added boosted vm :%s", vm_name);
}

//==================== FIFO
void print_boosted()
{
  n1 = STAILQ_FIRST(&boosted_head);
  while (n1 != NULL) {
    n2 = STAILQ_NEXT(n1, entries);
    log_trace(" %s \n", n1->vm_name);
    free(n1);
    n1 = n2;
  }
  STAILQ_INIT(&boosted_head);
}

void free_boosted()
{
  n1 = STAILQ_FIRST(&boosted_head);
  while (n1 != NULL) {
    n2 = STAILQ_NEXT(n1, entries);
    free(n1);
    n1 = n2;
  }
  STAILQ_INIT(&boosted_head);
}

void plug_virtual_function(const char *name);

void unplug_device(const char *name);

/**
 * unplug vf from guest in order to be allow pluging the vf to another
 */
void unplug_any_boosted_vm()
{
  struct vm_entry *tmp;

  if (STAILQ_EMPTY(&boosted_head))
    return;

  tmp = STAILQ_FIRST(&boosted_head);
  assert(tmp != NULL);

  unplug_device(tmp->vm_name);

  assert(tmp != NULL && "qeue empty");
  STAILQ_REMOVE_HEAD(&boosted_head, entries);
  // log_debug("[%s] vf desallocated to %s !\n", __func__, tmp->vm_name);
  free(tmp->vm_name);
  free(tmp);
}

void send_sig(const char *SIG, const char *ip, int pid)
{
  int sockfd;
  char *cmd = malloc(255);
  struct sockaddr_in servaddr;

  // Creating socket file descriptor
  if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
    log_error("socket creation failed");
    exit(EXIT_FAILURE);
  }

  memset(&servaddr, 0, sizeof(servaddr));

  // Filling server information
  servaddr.sin_family = AF_INET;
  servaddr.sin_port = htons(5005);
  servaddr.sin_addr.s_addr = inet_addr(ip);

  int n, len;
  sprintf(cmd, "%s %d%c", SIG, pid, '\0');
  sendto(sockfd, (const char *)cmd, strlen(cmd), MSG_CONFIRM,
         (const struct sockaddr *)&servaddr, sizeof(servaddr));
  close(sockfd);
  free(cmd);
}

/**
 * Get top vm in term of bytes send where bytes send > BOSOT_TREHHOLD
 * If exist boosted vms, unplug vf and plug it to needy vm
 */
void plug_vf_ifneed(struct rb_root *root)
{
  // log_trace("enter %s ", __func__);
  struct rb_node *node = NULL;
  char *ip = NULL;
  double s1, s2;

  // get top vm
  LOCK(&futex_var);

  struct stats_node *data = rb_entry(rb_first(root), struct stats_node, node);
  if (!data) {
    log_warn("tree is empty");
    goto end;
  }

  if (data->stat.bytes_send < BOSOT_TREHHOLD) {
    log_info("no action tresh hold is low");
    goto end;
  }

  // 1. si c'est la même vm et que le quantum n'est pas fini on laisse
  unplug_any_boosted_vm();
  // printf("find #%s# \n", data->stat.ip);
  sz_data_t *vm = table_get(vms_table, data->stat.ip);
  assert(vm != NULL && "vm is null");
  // plug vm

  s1 = elapsed_realtime();
  send_sig("STOP", data->stat.ip, data->stat.pid);
  plug_virtual_function(vm->value);
  send_sig("CONT", data->stat.ip, data->stat.pid);
  s2 = elapsed_realtime();
  duration(s1, s2);

  add_boosted(vm->value);
  // update tree
  rb_erase(&data->node, root);
  // free(data->stat.ip);
  free_memory(data);
end:
  UNLOCK(&futex_var);

  //  log_trace("exit %s ", __func__);
}

void clean_tree(uv_idle_t *handle);
/**
 *  handle guest stats and insert it to rbtree
 *   skip if treshhold not reached
 */
void process_guest_stats(uv_stream_t *client, ssize_t nread,
                         const uv_buf_t *buf)
{
  if (nread < 0) {
    if (nread != UV_EOF) {
      log_error("Read error %s\n", uv_err_name(nread));
      uv_close((uv_handle_t *)client, NULL);
    }
  }
  else if (nread > 0) {
    uv_write_t *req = (uv_write_t *)malloc(sizeof(uv_write_t));
    uv_buf_t wrbuf = uv_buf_init(buf->base, nread);
    /*uv_write(req, client, &wrbuf, 1, echo_write);
     */
    int i;
    int n = *(int *)buf->base;

    for (i = 0; i < n; i += 2) {

      if (*(float *)(buf->base + sizeof(int) + (i + 1) * sizeof(float)) <
          BOSOT_TREHHOLD) {
        continue;
      }

      struct stats_node *elem = malloc(sizeof(struct stats_node));
      elem->stat.pid = *(float *)(buf->base + sizeof(int) + i * sizeof(float));
      elem->stat.bytes_send =
          *(float *)(buf->base + sizeof(int) + (i + 1) * sizeof(float));
      elem->stat.ip = get_peername((uv_tcp_t *)client);

      LOCK(&futex_var);
      if (insert(&stats_tree, elem)) {
        table_update_inc_count(vms_table, elem->stat.ip);
        // print_stats(&stats_tree);
        // clean_tree(NULL);
      }

      UNLOCK(&futex_var);
      // log_debug("%s->%d inserted ",elem->stat.ip, elem->stat.bytes_send);
    }
  }
end:
  if (buf->base) {
    free(buf->base);
  }
  uv_close((uv_handle_t *)client, NULL);
}

void alloc_buffer(uv_handle_t *handle, size_t suggested_size, uv_buf_t *buf)
{
  buf->base = (char *)malloc(suggested_size);
  buf->len = suggested_size;
}

void echo_write(uv_write_t *req, int status)
{
  if (status) {
    log_error("Write error %s\n", uv_strerror(status));
  }
  free(req);
}

/**
 * Handle new guest connection
 */
void on_new_connection(uv_stream_t *server, int status)
{

  if (status < 0) {
    log_error("New connection error %s\n", uv_strerror(status));
    return;
  }

  uv_tcp_t *client = (uv_tcp_t *)malloc(sizeof(uv_tcp_t));
  uv_tcp_init(loop, client);

  if (uv_accept(server, (uv_stream_t *)client) == 0) {
    uv_read_start((uv_stream_t *)client, alloc_buffer, process_guest_stats);
  }
  else {
    uv_close((uv_handle_t *)client, NULL);
  }
}

void on_uv_close(uv_handle_t *handle)
{
  if (handle != NULL) {
    log_trace("close handle :%p", handle);
  }
}

void on_uv_walk(uv_handle_t *handle, void *arg)
{
  uv_close(handle, on_uv_close);
}

void on_sigint_received(uv_signal_t *handle, int signum)
{
  int result = uv_loop_close(handle->loop);
  if (result == UV_EBUSY) {
    uv_walk(handle->loop, on_uv_walk, NULL);
    free(handle);
  }
  log_debug("C-c received \n");
}

/**
 *  Decision scheduler
 */
void virtual_function_sheduler(uv_timer_t *timer, int status)
{
  log_trace("[%s] trigger", __func__);
  /*double s1, s2;
  s1 = elapsed_realtime();
  */
  plug_vf_ifneed(&stats_tree);
  /* s2 = elapsed_realtime();
  duration(s1,s2);
  */
}

/**
 *  build vm table learn from bridge
 */
void build_table()
{
  FILE *fp = NULL;
  char out[1024];
  char *line = NULL;
  char *token = NULL;
  const char *cmd = "./vm_tables.sh";
  log_trace("build vm tables ... please wait \n");

  vms_table = table_create();

  // plug vm
  fp = popen(cmd, "r");

  if (fp == NULL) {
    log_error("Failed to run command\n");
    return;
  }

  while (fgets(out, 1024, fp)) {
    line = &out[0];
    token = strtok(line, ",");
    sz_data_t *data = malloc(sizeof(sz_data_t));
    data->value = strdup(token);
    data->size = strlen(token);
    data->count = 0;
    table_add(vms_table, g_strdup(strtok(NULL, ",")), data);
  }
  // table_print(vms_table);
  log_trace("vms tables done \n");
}

int erase(struct rb_root *root, char *key, int count)
{
  struct rb_node *node;
  struct stats_node *pos;

  for (node = rb_last(root); node && count > 3; node = rb_prev(node)) {
    pos = rb_entry(node, struct stats_node, node);
    if (strcmp(pos->stat.ip, key) == 0) {
      count--;
      rb_erase(&pos->node, root);
    }
  }
  return count;
}

void clean_tree(uv_idle_t *handle)
{
  // if (1) return;
  guint i = 10;
  int n;
  struct stats_node *data;
  // log_debug("enter %s\n",__func__);
  LOCK(&futex_var);
  char **keys = (char **)g_hash_table_get_keys_as_array(vms_table, &i);
  for (int k = 0; k < i; k++) {

    sz_data_t *d = table_get(vms_table, keys[k]);
    if (d->count > 3) {
      n = erase(&stats_tree, keys[k], d->count);
      table_update_dec_count(vms_table, keys[k], d->count - n);
    }
  }

  UNLOCK(&futex_var);
  // sleep(1);
  // log_debug("exit %s\n",__func__);
}

static void test_tree(int bytes)
{
  guint i = 10;
  struct stats_node *elem = malloc(sizeof(struct stats_node));
  char **keys = (char **)g_hash_table_get_keys_as_array(vms_table, &i);
  elem->stat.pid = 1234;
  elem->stat.bytes_send = bytes;
  elem->stat.ip = keys[rand() % 2];
  LOCK(&futex_var);
  insert(&stats_tree, elem);
  table_update_inc_count(vms_table, elem->stat.ip);
  UNLOCK(&futex_var);
}

static void run_test_tree()
{
  build_table();
  time_t t;
  srand((unsigned)time(&t));
  for (int i = 0; i < 100; i++)
    test_tree(rand() % 50000 + 433);

  print_stats(&stats_tree);
  table_print(vms_table);
  clean_tree(NULL);
  print_stats(&stats_tree);
  table_print(vms_table);
}

//=================== libvirtd

void openConnection() { hypervisor = virConnectOpen("qemu:///system"); }

void plug_virtual_function(const char *name)
{
  virDomainPtr domain = virDomainLookupByName(hypervisor, name);
  virDomainAttachDeviceFlags(domain, interface, VIR_DOMAIN_AFFECT_CURRENT);
}

void unplug_device(const char *name)
{
  virDomainPtr domain = virDomainLookupByName(hypervisor, name);
  // virDomainDetachDeviceFlags(domain, interface, VIR_DOMAIN_AFFECT_CURRENT);
  virDomainDetachDeviceAlias(domain, interface, VIR_DOMAIN_AFFECT_CURRENT);
}

void listDomains()
{
  virDomainPtr domain;
  int count = virConnectNumOfDomains(hypervisor);
  int *ids = malloc(sizeof(int) * count);
  virConnectListDomains(hypervisor, ids, count);
  double s1, s2;
  for (int i = 0; i < count; i++) {
    domain = virDomainLookupByID(hypervisor, ids[i]);
    const char *name = virDomainGetName(domain);
    printf("name of domain %d is %s\n", ids[i], name);
  }
}

//=================== libvirtd

static void funtest(char **argv)
{
  send_sig("STOP", "192.168.122.115", 6258);
  int n = atoi(argv[1]);
  if (n == 0)
    unplug_device(argv[2]);
  else
    plug_virtual_function(argv[2]);
  send_sig("CONT", "192.168.122.115", 6258);
  // udevadm control --reload-rules && udevadm trigger
  exit(1);
}
int main(int argc, char **argv)
{

  log_trace("stating flexvf \n");
  openConnection();

  funtest(argv);

  // run_test_tree();

  uv_timer_t timer1;
  build_table();

  loop = uv_default_loop();

  uv_signal_t *sigint = malloc(sizeof(uv_signal_t));
  uv_signal_init(loop, sigint);
  uv_signal_start(sigint, on_sigint_received, SIGINT);

  uv_timer_init(loop, &timer1);
  uv_timer_start(&timer1, (uv_timer_cb)&virtual_function_sheduler, 5000,
                 SCHED_FREQ);

  // uv_idle_t idler;
  // uv_idle_init(loop, &idler);
  // uv_idle_start(&idler, clean_tree);

  uv_tcp_t server;
  uv_tcp_init(loop, &server);
  bzero(addr.sin_zero, 8);
  uv_ip4_addr(SERVER, PORT, &addr);
  addr.sin_port = htons(PORT);

  uv_tcp_bind(&server, (const struct sockaddr *)&addr, 0);
  int r = uv_listen((uv_stream_t *)&server, 128, on_new_connection);
  if (r) {
    fprintf(stderr, "Listen error %s\n", uv_strerror(r));
    return 1;
  }

  return uv_run(loop, UV_RUN_DEFAULT);

  log_trace("end flexvf \n");
}
