#from fabric import Connection
#from fabric import SerialGroup as Group
from fabric import task


@task
def update(c):
    c.run('sudo apt-get update')


@task
def install_package(c):
    c.run(
        "sudo apt-get install -y qemu-kvm libvirt-bin virtinst bridge-utils libguestfs-tools virt-top"
    )


@task
def config_libvirtd(c):
    c.put('qemu.conf', '/tmp')
    c.run('sudo mv /tmp/qemu.conf', '/etc/libvirt')


@task
def runall(c):
    update(c)
    install_package(c)
    config_libvirtd(c)


#pool = Group('','')
#pool.run('')
