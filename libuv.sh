#!/bin/bash

if [ ! -f libuv-v1.38.0 ]
then
    wget -O- https://dist.libuv.org/dist/v1.38.0/libuv-v1.38.0.tar.gz | tar xzvf -
fi

case $1 in
    yes)
        exit ;;
esac;

mkdir libuv-v1.38.0/build

pushd libuv-v1.38.0/build

cmake ..

make -j 32  && make install

exit 0
