#!/bin/bash

set -e

function teamd_rules {
cat>/tmp/999-eth.rules<<EOF
KERNEL=="eth0",
KERNELS=="0000:00:07.0",
SUBSYSTEMS=="pci",
DRIVERS=="ixgbevf",
ATTRS{device}=="0x10ed",
ATTRS{subsystem_device}=="0x000c",
ATTRS{subsystem_vendor}=="0x8086",
ATTRS{vendor}=="0x8086",
ACTION=="add", RUN+="/usr/bin/teamd_hotplug add"
KERNEL=="eth0", ACTION=="remove", RUN+="/usr/bin/teamd_hotplug remove"
EOF
}


#handle nic event and update teamd
function udev_teamd_handler
{
cat>/tmp/teamd_hotplug<<EOF
#!/bin/bash

#ip link set dev ens7 down

#sleep 0.3

case \$1 in
add)
    teamdctl team0 port add eth0
    ip link set dev eth0 up
        ip link set ens2f1 dev down
    teamdctl team0 port remove ens2f1
;;
remove)
    teamdctl team0 port add ens2f1
    ip link set dev ens2f1 up
        ip link set eth0 dev down
    teamdctl team0 port remove eth0
;;
esac;
exit 0
EOF
chmod +x /tmp/teamd_hotplug
}

# create teamd confi
function team_config {
cat>/tmp/team0.conf<<EOF
{
   "device": "team0",
   "runner": {"name": "activebackup"},
   "link_watch": {"name": "ethtool"},
   "ports": {
      "eth0": {
         "prio": -10,
         "sticky": true
       },
       "ens7": {
         "prio": 100
       }
   }
}
EOF
cat>/tmp/ifcfg-team0-slave0<<EOF
NAME=team0-eth0
DEVICE=eth0
ONBOOT=yes
NM_CONTROLLED=no
TEAM_MASTER=team0
BOOTPROTO=none
DEVICETYPE=TeamPort
TEAM_CONFIG='{"prio":100,"sticky":true}'
EOF

cat>/tmp/ifcfg-team0-slave1<<EOF
NAME=team0-ens7
DEVICE=ens7
ONBOOT=yes
TEAM_MASTER=team0
NM_CONTROLLED=no
BOOTPROTO=none
DEVICETYPE=TeamPort
TEAM_CONFIG='{"prio":100}'
EOF


cat>/tmp/ifcfg-team0<<EOF
DEVICE=team0
NAME=team0
DEVICETYPE=Team
ONBOOT=yes
BOOTPROTO=dhcp
NM_CONTROLLED="no"
TEAM_CONFIG='{"runner": {"name": "activebackup"}, "link_watch": {"name": "ethtool"}}'
EOF
}

function ssh_config {
cat>/tmp/sshd_config<<EOF
UsePAM yes

X11Forwarding yes
PrintMotd no
AcceptEnv LANG LC_*

# override default of no subsystems
Subsystem   sftp    /usr/lib/openssh/sftp-server

Protocol 2
PasswordAuthentication yes
ChallengeResponseAuthentication no
PermitRootLogin yes
EOF
}

function netplan {
cat>/tmp/01-netcfg.yaml<<EOF
network:
  version: 2
  renderer: networkd
  ethernets:
    ens2f0:
      dhcp4: yes
EOF
}

# build virtual machine
function build {

    echo toto >/tmp/rootpwd
    team_config
    udev_teamd_handler
    teamd_rules
    ssh_config
    netplan

    for vm in vm
    do
        virt-builder ubuntu-18.04 \
            -o ${vm}_disk.img \
            --size 20G \
            --root-password file:/tmp/rootpwd \
            --hostname $vm \
            --install "vim,git,ssh,ethtool,build-essential,openjdk-8-jdk,openjdk-8-jre,autoconf,libgoogle-perftools-dev,zlib1g-dev,libboost-all-dev,libreadline-dev,bison,libsphinxbase-dev,libpocketsphinx-dev,uuid-dev,libteamdctl0,libteam-utils,bpfcc-tools,libopencv-dev,g++-4.8,gcc-4.8,libgtop2-dev,libncurses5-dev,libjemalloc-dev,libdb++-dev,libssl-dev,libaio-dev,libcrypto++-dev, python-pip" \
            --copy-in /tmp/teamd_hotplug:/usr/bin \
            --copy-in /tmp/team0.conf:/etc \
            --copy-in /tmp/999-eth.rules:/etc/udev/rules.d \
            --copy-in /tmp/sshd_config:/etc/ssh \
            --copy-in /tmp/01-netcfg.yaml:/etc/netplan \
        --upload /mydata/smart_io_alloc/dev.tar:/root/dev.tar \
        --firstboot-command 'dhclient -v $(ip a s | grep ^2: | cut -d: -f 2); apt-get remove gcc-7 g++-7; pip install future-fstrings'
    done
}

function import_vm {
vm=$1
virt-install \
    --virt-type=kvm \
    --noacpi \
    --noapic \
    --name $vm \
    --ram 8192 \
    --vcpus=4 \
    --os-variant=rhel7 \
    --network network=default,model=virtio \
    --nographics  \
    --disk path=./$vm_disk.img,size=2,bus=virtio \
    --update
}

case $1 in
    build)
        build
        ;;
    import)
        import_vm $2
        ;;
esac;

exit 0
