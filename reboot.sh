#!/bin/bash

sudo kexec -l /boot/vmlinuz-4.15.0-101-generic --initrd=/boot/initrd.img-`uname -r` --command-line="root=UUID=5fac4c69-51f6-47af-9773-a0d722426942 ro console=ttyS0,115200 intel_iommu=on"

sudo kexec -e

exit 0

