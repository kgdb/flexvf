import re

wordList = None

with open('articles.txt', 'r') as f:
    mystr = f.read().replace('\n', '')
    wordList = re.sub("[^\w]", " ", mystr).split()

with open('terms.in', 'w') as f:
    f.write("\n".join(wordList))
