#!/bin/bash

if [[ -z "${NTHREADS}" ]]; then NTHREADS=1; fi

QPS=1000 \
MAXREQS=3000 \
WARMUPREQS=14000 \
TBENCH_MAXREQS=${MAXREQS} TBENCH_WARMUPREQS=${WARMUPREQS} chrt -r 99 \
    ./mttest_server_networked -j${NTHREADS} mycsba masstree &


pid=$!

wait $pid

kill -9 $pid >/dev/null 2>&1


