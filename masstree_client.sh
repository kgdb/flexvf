#!/bin/bash

while read line
do

target=$(cut -d ',' -f 1 <<< $line) 
ip=$(cut -d ',' -f 2 <<< $line) 

cat>vm_masstree_${target}.sh<<EOF	
#!/bin/bash

TBENCH_SERVER=${ip} \
TBENCH_SERVER_PORT=8080 \
TBENCH_CLIENT_THREADS=400 \
TBENCH_QPS=1000 \
TBENCH_MINSLEEPNS=10000 \
TBENCH_RANDSEED=10 \
./mttest_client_networked

exit 0


EOF
chmod +x vm_masstree_${target}.sh

done <<< $(./vm_tables.sh)

exit 0

