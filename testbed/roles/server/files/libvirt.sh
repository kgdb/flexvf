#!/bin/bash

echo $1 > /tmp/virt.txt
case $1 in
    yes)
        exit 0
        ;;
esac;

export PERL_MM_USE_DEFAULT=1

perl -MCPAN -e 'install Pod::Man'
perl -MCPAN -e 'install Pod::Simple'

if [ ! -f libvirt ]
then
    git clone https://github.com/libvirt/libvirt.git
    cd libvirt
    git checkout v6.6.0-rc1
    cd ..
fi


mkdir libvirt/build

pushd libvirt/build
git checkout v6.6.0-rc1
../autogen.sh --system
../configure --prefix=/usr --with-qemu --with-qemu-user=libvirt-qemu --with-qemu-group=kvm --with-dnsmasq-path=/usr/sbin/dnsmasq


make -j 64 && make install

popd
systemctl enable libvirtd.service
systemctl disable systemd-resolved
systemctl stop systemd-resolved

